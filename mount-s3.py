import tkinter as tk

import subprocess
from tkinter import messagebox
from os.path import expanduser
import os


def mount():

    home = expanduser("~")

    mount_dir = home + "/" + bucket_entry.get()

    if not os.path.exists(mount_dir):
        os.makedirs(mount_dir)


    pass_file = home + "/.passwd-s3fs"
    passwd_s3fs = open(pass_file,"w")
    passwd_s3fs.write(access_entry.get()+":" + secret_entry.get())
    passwd_s3fs.close()

    subprocess.call(['chmod','0600',pass_file])

    if access_entry.get() !="" and secret_entry.get() !="":
        public_flg = 0
    else:
        public_flg = 1

    if os.path.ismount(mount_dir):
        messagebox.showerror(title="Error", message="Bucket is already mounted")
    else:
        subprocess.call(['mkdir', '-p',mount_dir])

        if public_flg ==0:
            if loc_var.get() =="EMBL":
                p1 = subprocess.Popen(["s3fs",bucket_entry.get(), mount_dir, "-o","url=https://s3.embl.de","-o","use_path_request_style"],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            elif loc_var.get()=="Others":
                p1 = subprocess.Popen(["s3fs", bucket_entry.get(), mount_dir, "-o", "url="+endpoint.get(), "-o", "use_path_request_style"],stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            else:
                p1 = subprocess.Popen(
                    ["s3fs", bucket_entry.get(), mount_dir],stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        else:
            if loc_var.get() =="EMBL":
                p1 = subprocess.Popen(["s3fs",bucket_entry.get(), mount_dir, "-o","url=https://s3.embl.de","-o","use_path_request_style","-o","public_bucket=1"],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            elif loc_var.get()=="Others":
                p1 = subprocess.Popen(["s3fs", bucket_entry.get(), mount_dir, "-o", "url="+endpoint.get(), "-o", "use_path_request_style","-o","public_bucket=1"],stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            else:
                p1 = subprocess.Popen(
                    ["s3fs", bucket_entry.get(), mount_dir,"-o","public_bucket=1"],stdout=subprocess.PIPE, stderr=subprocess.PIPE)


        output,err = p1.communicate()

        if err.decode("utf-8") == "":
            if os.path.ismount(mount_dir):
                messagebox.showinfo("Success!!", "Your S3 bucket is mounted!")
                subprocess.check_call(['xdg-open', mount_dir])
            else:
                messagebox.showerror("Error!!","Error mounting S3 bucket, please check your credentials and try again !")
        else:
            messagebox.showerror("Error!!",err+"Error mounting S3 bucket, please check your credentials and try again !")



def _get(*args):
    if loc_var.get()=="Others":
        endpoint.grid()
        typelbl.grid(row=1)
    else:
        endpoint.grid_remove()
        typelbl.grid_forget()


master = tk.Tk()
master.title("EMBL S3 client")

OptionList = [
    "EMBL",
    "AWS",
    "Others"
]

loc_var =tk.StringVar()
loc_var.set(OptionList[0])
#sv=OptionList[0]

location = tk.OptionMenu(master,loc_var,*OptionList, command=_get)
location.config(width=20)
location.grid(row=0,column=1,sticky="ew")
# location.pack(side="top")


tk.Label(master,text="S3 types").grid(row=0)
typelbl=tk.Label(master,text="Endpoint")
typelbl.grid(row=1)
tk.Label(master,text="Access Key").grid(row=2)
tk.Label(master,text="Secret Key").grid(row=3)
tk.Label(master,text="Bucket").grid(row=4)

endpoint = tk.Entry(master,width=30)
typelbl.grid_forget()
access_entry = tk.Entry(master,width=30)
secret_entry = tk.Entry(master,width=30)
bucket_entry = tk.Entry(master,width=30)

endpoint.grid(row=1,column = 1,padx=5,pady=5)
access_entry.grid(row=2,column = 1,padx=5,pady=5)
secret_entry.grid(row=3, column=1,padx=5,pady=5)
bucket_entry.grid(row=4,column=1,padx=5,pady=5)

endpoint.grid_remove()
tk.Button(master, text='Quit',command=master.quit).grid(row=5, column=0, sticky=tk.W, pady=4)
tk.Button(master, text='Mount', command=mount).grid(row=5, column=1, sticky=tk.W, pady=4)

master.mainloop()
